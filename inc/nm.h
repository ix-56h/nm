#ifndef __NM__
# define __NM__

# include <stdio.h>
# include <elf.h>
# include <sys/stat.h>
# include <sys/mman.h>

# define LITERAL(name) #name
# define EI_CLASS_OFFSET 0x4

// http://forum.codecall.net/topic/56591-bit-fields-flags-tutorial-with-example/
enum options {
    DISPLAY_HEADER  = 0x01,
    DISPLAY_DYN     = 0x02,

};

typedef enum e_bool {FALSE, TRUE} boolean;

static const char a_arch[2][10] = { [0] = "x86", [1] = "x64"};
static const char a_endianess[2][14] = { [0] = "Little Endian", [1] = "Big Endian"};

typedef struct s_elf64{
	Elf64_Ehdr  *Ehdr;
	Elf64_Shdr  *Shdr;
    Elf64_Shdr  *Shstr; 
    Elf64_Shdr  *Dynsym; 
}               elf64;

typedef struct s_elf32{
	Elf32_Ehdr  *Ehdr;
	Elf32_Shdr  *Shdr;
    Elf32_Shdr  *Shstr; 
    Elf32_Shdr  *Dynsym; 
}               elf32;

typedef struct s_elfH
{
    int fd;
    struct stat stats;
    elf64       elf64;
    elf32       elf32;
    char        *Strtab; 
    char        *Dynstr; 
    char        *Shstrtab; 
    void        *basePtr;
    void        *sectionsStart;
    boolean     arch;
    boolean     endianess;
    unsigned int opts;
    const char  *path;
} elfH;

extern elfH g_elfH;

// utils.c
boolean ELF_Magic(char *head);
uint32_t swp32(uint32_t num);
boolean cmpn(char *foo, const char *bar, size_t n);
char *addOf(boolean endian, char *base, int off);

// elfparser86.c
const char *get_literal32(int type);
Elf32_Shdr *get_section32(size_t type);
Elf32_Shdr *get_section_by_name32(const char *name);
void display_symbols32(Elf32_Shdr *section);
void display_section_table32();
void display_elf32();
 
// elfparser64.c
const char *get_literal(int type);
Elf64_Shdr *get_section(size_t type);
Elf64_Shdr *get_section_by_name(const char *name);
void display_symbols(Elf64_Shdr *section);
void display_section_table();
void display_elf();

int init_elf(const char* path);
void display_elf();

#endif