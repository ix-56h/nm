#include <fcntl.h>
#include <unistd.h>
#include "nm.h"

elfH g_elfH;

int init_elf(const char* path)
{
	g_elfH.fd = open(path, O_RDONLY);
	if (g_elfH.fd == -1)
	{
		printf("ftnm: '%s' No such file\n", path);
		return -1;
	}
	fstat(g_elfH.fd, &g_elfH.stats);
   	g_elfH.basePtr = mmap(NULL, g_elfH.stats.st_size, PROT_READ, MAP_PRIVATE, g_elfH.fd, 0x0);
	if (!ELF_Magic(g_elfH.basePtr))
	{
		printf("ftnm: %s: not ELF\n", path);
		close(g_elfH.fd);
		return -1;
	}
	g_elfH.arch = *(char*)(g_elfH.basePtr + EI_CLASS_OFFSET) == 2;
	g_elfH.endianess = *(char*)(g_elfH.basePtr + EI_DATA) == 2;
	if (g_elfH.arch == 0)
	{
	    g_elfH.elf32.Ehdr = (Elf32_Ehdr*)g_elfH.basePtr;	
   		g_elfH.sectionsStart = g_elfH.basePtr + g_elfH.elf32.Ehdr->e_shoff;	
    	g_elfH.elf32.Shdr = g_elfH.sectionsStart;
	}
	else
	{
	    g_elfH.elf64.Ehdr = (Elf64_Ehdr*)g_elfH.basePtr;	
   		g_elfH.sectionsStart = g_elfH.basePtr + g_elfH.elf64.Ehdr->e_shoff;	
    	g_elfH.elf64.Shdr = g_elfH.sectionsStart;
	}
	g_elfH.path = path;
	close(g_elfH.fd);
	return 0;
}

int get_options(unsigned int *opts, char **av, int ac)
{
	for (int i = 0; i < ac; i++)
	{
		if (av[i][0] == '-')
		{
			switch (av[i][1])
			{
			case 'd':
				*opts |= DISPLAY_DYN;
				break;
			case 'a':
				*opts |= DISPLAY_HEADER;
				break;
			default:
				printf("Unknown option \"%s\"", av[i]);
				return -1;
			}
		}
	}
	return 0;
}

int main(int ac, char **av)
{
	if (ac >= 2 && get_options(&g_elfH.opts, av, ac) == -1)
		return -1;
	int ret = 0;
	//fetch all args and run nm for each
	if (ac >= 2)
	{
		for (int i = 1; i < ac; i++)
		{
			if (av[i][0] != '-')
			{
				ret = init_elf(av[i]);
				if (ret == -1) continue ;
				
				if (!ELF_Magic(g_elfH.basePtr))
				{
					munmap(g_elfH.basePtr, g_elfH.stats.st_size);
					continue ;
				}
				ac > 2 ? printf("\n%s:\n", av[i]) : ac;
				if (g_elfH.opts & DISPLAY_HEADER)
					g_elfH.arch ? display_elf() : display_elf32();
				
				g_elfH.arch ? display_section_table() : display_section_table32();
				munmap(g_elfH.basePtr, g_elfH.stats.st_size);
			}	
		}
	}	
	else
	{
		ret = init_elf("a.out");
		if (ret == -1) return -1;
		if (!ELF_Magic(g_elfH.basePtr))
		{
			munmap(g_elfH.basePtr, g_elfH.stats.st_size);
			return -1;
		}

		if (g_elfH.opts & DISPLAY_HEADER)
			g_elfH.arch ? display_elf() : display_elf32();
		
		g_elfH.arch ? display_section_table() : display_section_table32();
		munmap(g_elfH.basePtr, g_elfH.stats.st_size);
	}

	return 0;
}
