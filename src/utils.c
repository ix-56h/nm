#include "nm.h"

uint32_t swp32(uint32_t num)
{
	uint32_t swapped = 
			((num>>24)&0xff) | // move byte 3 to byte 0
			((num<<8)&0xff0000) | // move byte 1 to byte 2
			((num>>8)&0xff00) | // move byte 2 to byte 1
			((num<<24)&0xff000000); // byte 0 to byte 3
	return swapped;
}

boolean cmpn(char *foo, const char *bar, size_t n)
{
	for (size_t i = 0; i < n; i++)
	{
		if (foo[i] != bar[i])
			return FALSE;
	}
	return TRUE;
}

boolean ELF_Magic(char *head)
{
	if (cmpn(head, ELFMAG, 4))
		return TRUE;
	return FALSE;
}

const char *get_literal(int type)
{
	switch (type)
	{
		case 0:
			return LITERAL(SHT_NULL);
		case 1:
			return LITERAL(SHT_PROGBITS);
		case 2:
			return LITERAL(SHT_SYMTAB);
		case 3:
			return LITERAL(SHT_STRTAB);
		case 4:
			return LITERAL(SHT_RELA);
		case 5:
			return LITERAL(SHT_HASH);
		case 6:
			return LITERAL(SHT_DYNAMIC);
		case 7:
			return LITERAL(SHT_NOTE);
		case 8:
			return LITERAL(SHT_NOBITS);
		case 9:
			return LITERAL(SHT_REL);;
		case 10:
			return LITERAL(SHT_SHLIB);
		case 11:
			return LITERAL(SHT_DYNSYM);
		case 14:
			return LITERAL(SHT_INIT_ARRAY);
		case 15:
			return LITERAL(SHT_FINI_ARRAY);
		case 16:
			return LITERAL(SHT_PREINIT_ARRAY);
		case 17:
			return LITERAL(SHT_GROUP);
		case 18:
			return LITERAL(SHT_SYMTAB_SHNDX);
		case 19:
			return LITERAL(SHT_NUM);
		case 0x60000000:
			return LITERAL(SHT_LOOS);
		case 0x6ffffff5:
			return LITERAL(SHT_GNU_ATTRIBUTES);
		case 0x6ffffff6:
			return LITERAL(SHT_GNU_HASH);
		case 0x6ffffff7:
			return LITERAL(SHT_GNU_LIBLIST);
		case 0x6ffffff8:
			return LITERAL(SHT_CHECKSUM);
		case 0x6ffffffa:
			return LITERAL(SHT_LOSUNW);
		case 0x6ffffffb:
			return LITERAL(SHT_SUNW_COMDAT);
		case 0x6ffffffc:
			return LITERAL(SHT_SUNW_syminfo);
		case 0x6ffffffd:
			return LITERAL(SHT_GNU_verdef);
		case 0x6ffffffe:
			return LITERAL(SHT_GNU_verneed);
		case 0x6fffffff:
			return LITERAL(SHT_GNU_versym);
		case 0x70000000:
			return LITERAL(SHT_LOPROC);
		case 0x7fffffff:
			return LITERAL(SHT_HIPROC);
		case 0x80000000:
			return LITERAL(SHT_LOUSER);
		case 0x8fffffff:
			return LITERAL(SHT_HIUSER);
	default:
		break;
	}
	return NULL;

}