#include <fcntl.h>
#include <unistd.h>
#include "nm.h"

Elf32_Shdr *get_section32(size_t type)
{
	for (size_t i = 0; i < g_elfH.elf32.Ehdr->e_shnum; i++)
	{
		if (g_elfH.elf32.Shdr[i].sh_type == type)
			return &g_elfH.elf32.Shdr[i];
	}
	return NULL;
}

Elf32_Shdr *get_section_by_name32(const char *name)
{
	if (g_elfH.elf32.Ehdr->e_shoff > g_elfH.stats.st_size)
		return NULL;
	g_elfH.Shstrtab = (char*)g_elfH.basePtr + g_elfH.elf32.Shdr[g_elfH.elf32.Ehdr->e_shstrndx].sh_offset;
	for (size_t i = 0; i < g_elfH.elf32.Ehdr->e_shnum; i++)
	{
		char *s_name = g_elfH.Shstrtab + g_elfH.elf32.Shdr[i].sh_name;
		if (cmpn(s_name, name, 7))
			return &g_elfH.elf32.Shdr[i];
	}
	return NULL;
}

static inline void display_type32(Elf32_Sym sym)
{
	char l = '?';
	boolean local = (ELF32_ST_BIND(sym.st_info) == STB_LOCAL);
	int x = 0;
	//STT = st_type
	//STB = BIN(st_info)
	//SHT = sectionheader[sym.st_shndx].sh_type
	if (ELF32_ST_BIND(sym.st_info) == STB_GNU_UNIQUE)
		l = 'u';
	else if (ELF32_ST_BIND(sym.st_info) == STB_WEAK && ELF32_ST_TYPE(sym.st_info) == STT_OBJECT)
		l = local == TRUE ? 'v' : 'V';
	else if (ELF32_ST_BIND(sym.st_info) == STB_WEAK && (sym.st_value == 0 || local == TRUE))
        l = 'w';
	else if (ELF32_ST_BIND(sym.st_info) == STB_WEAK && local == FALSE)
		l = 'W';
	else if (sym.st_shndx == SHN_UNDEF)
		l = 'U';
	else if (sym.st_shndx == SHN_ABS)
		l = local == TRUE ? 'a' : 'A';
	else if (sym.st_shndx == SHN_COMMON)
		l = 'C';
	else if (g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_NOBITS && g_elfH.elf32.Shdr[sym.st_shndx].sh_flags == (SHF_ALLOC | SHF_WRITE))
		l = local == TRUE ? 'b' : 'B';
	else if ((g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS 
			|| g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_FINI_ARRAY
			|| g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_INIT_ARRAY
			|| g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_DYNAMIC
			)
			&& g_elfH.elf32.Shdr[sym.st_shndx].sh_flags == (SHF_ALLOC | SHF_WRITE))
		l = local == TRUE ? 'd' : 'D';
	else if (g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_NOTE && g_elfH.elf32.Shdr[sym.st_shndx].sh_flags == 0)
		l = local == TRUE ? 'd' : 'D';
	else if ((g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS
			|| g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_HASH
			|| g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_DYNSYM
			|| g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_HISUNW
			|| g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_GNU_verneed
			|| g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_STRTAB
			|| g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_REL
            || g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_NOTE)
            && g_elfH.elf32.Shdr[sym.st_shndx].sh_flags == SHF_ALLOC)
		l = local == TRUE ? 'r' : 'R';
	//else if ()
	//	l = 'i';
	//else if ()
	//	l = local == TRUE ? 's' : 'S';
	else if (g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS && g_elfH.elf32.Shdr[sym.st_shndx].sh_flags == (SHF_ALLOC | SHF_EXECINSTR))
		l = local == TRUE ? 't' : 'T';
	else if (g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS)
		l = 'N';
	else if (g_elfH.elf32.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS && g_elfH.elf32.Shdr[sym.st_shndx].sh_flags == SHF_ALLOC)
		l = 'p';
	if (l == '?')
	{
		x = g_elfH.elf32.Shdr[sym.st_shndx].sh_type;
		(void)x;
    	//printf("%x = %x\n", sym.st_value , x);
	}
	//else if ()
	//	l = 'G/g';
    printf(" %c ", l);

}

void display_symbols32(Elf32_Shdr *section)
{
	Elf32_Sym *sym = (Elf32_Sym*)(g_elfH.basePtr + section->sh_offset);
	int value = 0;
	(void)value;
	for (size_t i = 0; i * sizeof(Elf32_Sym) < section->sh_size; i++)
	{
		value = sym[i].st_value;
		//if (sym[i].st_shndx != 0 || sym[i].st_info != 0)
		if (sym[i].st_name != 0 && ELF32_ST_TYPE(sym[i].st_info) != STT_FILE)
        {
			(sym[i].st_value != 0 || sym[i].st_shndx == SHN_ABS) && sym[i].st_shndx != SHN_UNDEF ? printf("%08x", sym[i].st_value) : printf("%-8s", " ");
			display_type32(sym[i]);
			printf("%s\n", g_elfH.Strtab + sym[i].st_name);
		}
	}
}

void display_dynsymbols32(Elf32_Shdr *section)
{
	Elf32_Sym *sym = (Elf32_Sym*)(g_elfH.basePtr + section->sh_offset);
	for (size_t i = 0; i * sizeof(Elf32_Sym) < section->sh_size; i++)
	{
		if (sym[i].st_name != 0)
			printf("%#010x %i %s\n", sym[i].st_value, ELF32_ST_TYPE(sym[i].st_info), g_elfH.Dynstr + sym[i].st_name);
	}
	
}

void display_section_table32()
{
	Elf32_Shdr *shstr = get_section_by_name32(".strtab");
	if (shstr)
	{
		g_elfH.Strtab = (char*)g_elfH.basePtr + shstr->sh_offset;
		for (size_t i = 0; i < g_elfH.elf32.Ehdr->e_shnum; i++)
		{
			//if (g_elfH.elf32.Shdr[i].sh_type == SHT_DYNSYM)
			//	g_elfH.elf32.Dynsym = &g_elfH.elf32.Shdr[i];
			if (g_elfH.elf32.Shdr[i].sh_type == SHT_SYMTAB)
				display_symbols32(&g_elfH.elf32.Shdr[i]);
		}
	}
	else
		fprintf(stderr, "ftnm: %s: no symbols\n", g_elfH.path);
}

void display_elf32()
{
	printf("magics : ELF\n");
	printf("e_ident[EI_CLASS] = %s\ne_ident[EI_DATA] = %s\n", a_arch[g_elfH.elf32.Ehdr->e_ident[4] - 1], a_endianess[g_elfH.elf32.Ehdr->e_ident[5] - 1]);
	printf("Header Table start at %p\n", g_elfH.elf32.Ehdr);
	printf("Section Header Table head.e_shoff = %#04X\n", g_elfH.elf32.Ehdr->e_shoff);
	printf("Section start at %p\n", g_elfH.basePtr + g_elfH.elf32.Ehdr->e_shoff);
	printf("Section Header Table Size head.e_shentsize = %i\n", g_elfH.elf32.Ehdr->e_shentsize);
	printf("Section Header Table entry count = %i\n", g_elfH.elf32.Ehdr->e_shnum);
	printf("Section Header string table index %i\n", g_elfH.elf32.Ehdr->e_shstrndx);
}