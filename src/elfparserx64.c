#include <unistd.h>
#include "nm.h"

Elf64_Shdr *get_section(size_t type)
{
	for (size_t i = 0; i < g_elfH.elf64.Ehdr->e_shnum; i++)
	{
		if (g_elfH.elf64.Shdr[i].sh_type == type)
			return &g_elfH.elf64.Shdr[i];
	}
	return NULL;
}

Elf64_Shdr *get_section_by_name(const char *name)
{
	if (g_elfH.elf64.Ehdr->e_shoff > (unsigned long long)g_elfH.stats.st_size)
		return NULL;
	g_elfH.Shstrtab = (char*)g_elfH.basePtr + g_elfH.elf64.Shdr[g_elfH.elf64.Ehdr->e_shstrndx].sh_offset;
	for (size_t i = 0; i < g_elfH.elf64.Ehdr->e_shnum; i++)
	{
		char *s_name = g_elfH.Shstrtab + g_elfH.elf64.Shdr[i].sh_name;
		if (cmpn(s_name, name, 7))
			return &g_elfH.elf64.Shdr[i];
	}
	return NULL;
}

static inline void display_type(Elf64_Sym sym)
{
	char l = '?';
	boolean local = (ELF64_ST_BIND(sym.st_info) == STB_LOCAL);
	//STT = st_type
	//STB = BIN(st_info)
	//SHT = sectionheader[sym.st_shndx].sh_type
	if (ELF64_ST_BIND(sym.st_info) == STB_GNU_UNIQUE)
		l = 'u';
	else if (ELF64_ST_BIND(sym.st_info) == STB_WEAK && ELF64_ST_TYPE(sym.st_info) == STT_OBJECT)
		l = local == TRUE ? 'v' : 'V';
	else if (ELF64_ST_BIND(sym.st_info) == STB_WEAK && (sym.st_value == 0 || local == TRUE))
        l = 'w';
	else if (ELF64_ST_BIND(sym.st_info) == STB_WEAK && local == FALSE)
		l = 'W';
	else if (sym.st_shndx == SHN_UNDEF)
		l = 'U';
	else if (sym.st_shndx == SHN_ABS)
		l = local == TRUE ? 'a' : 'A';
	else if (sym.st_shndx == SHN_COMMON)
		l = 'C';
	else if (g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_NOBITS && g_elfH.elf64.Shdr[sym.st_shndx].sh_flags == (SHF_ALLOC | SHF_WRITE))
		l = local == TRUE ? 'b' : 'B';
	else if ((g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS 
			|| g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_FINI_ARRAY
			|| g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_INIT_ARRAY
			|| g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_DYNAMIC
			)
			&& g_elfH.elf64.Shdr[sym.st_shndx].sh_flags == (SHF_ALLOC | SHF_WRITE))
		l = local == TRUE ? 'd' : 'D';
	else if (g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_NOTE && g_elfH.elf64.Shdr[sym.st_shndx].sh_flags == 0)
		l = local == TRUE ? 'd' : 'D';
	else if ((g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS
            || g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_NOTE)
            && g_elfH.elf64.Shdr[sym.st_shndx].sh_flags == SHF_ALLOC)
		l = local == TRUE ? 'r' : 'R';
	//else if ()
	//	l = 'i';
	//else if ()
	//	l = local == TRUE ? 's' : 'S';
	else if (g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS && g_elfH.elf64.Shdr[sym.st_shndx].sh_flags == (SHF_ALLOC | SHF_EXECINSTR))
		l = local == TRUE ? 't' : 'T';
	else if (g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS)
		l = 'N';
	else if (g_elfH.elf64.Shdr[sym.st_shndx].sh_type == SHT_PROGBITS && g_elfH.elf64.Shdr[sym.st_shndx].sh_flags == SHF_ALLOC)
		l = 'p';
	//else if ()
	//	l = 'G/g';
    printf(" %c ", l);

}

void display_symbols(Elf64_Shdr *section)
{
	Elf64_Sym *sym = (Elf64_Sym*)(g_elfH.basePtr + section->sh_offset);
	for (size_t i = 0; i * sizeof(Elf64_Sym) < section->sh_size; i++)
	{
		//if (sym[i].st_shndx != 0 || sym[i].st_info != 0)
		if (sym[i].st_name != 0 && ELF64_ST_TYPE(sym[i].st_info) != STT_FILE)
        {
			(sym[i].st_value != 0 || sym[i].st_shndx == SHN_ABS) && sym[i].st_shndx != SHN_UNDEF ? printf("%016lx", sym[i].st_value) : printf("%-16s", " ");
			display_type(sym[i]);
			printf("%s\n", g_elfH.Strtab + sym[i].st_name);
		}
	}
	
}

void display_dynsymbols(Elf64_Shdr *section)
{
	Elf64_Sym *sym = (Elf64_Sym*)(g_elfH.basePtr + section->sh_offset);
	for (size_t i = 0; i * sizeof(Elf64_Sym) < section->sh_size; i++)
	{
		if (sym[i].st_name != 0)
			printf("%#010lx %i %s\n", sym[i].st_value, ELF64_ST_TYPE(sym[i].st_info), g_elfH.Dynstr + sym[i].st_name);
	}
	
}

void display_section_table()
{
	Elf64_Shdr *shstr = get_section_by_name(".strtab");
	if (shstr)
	{
		g_elfH.Strtab = (char*)g_elfH.basePtr + shstr->sh_offset;
		for (size_t i = 0; i < g_elfH.elf64.Ehdr->e_shnum; i++)
		{
			if (g_elfH.elf64.Shdr[i].sh_type == SHT_DYNSYM)
				g_elfH.elf64.Dynsym = &g_elfH.elf64.Shdr[i];
			if (g_elfH.elf64.Shdr[i].sh_type == SHT_SYMTAB)
				display_symbols(&g_elfH.elf64.Shdr[i]);
		}
	}
	else
		fprintf(stderr, "ftnm: %s: no symbols\n", g_elfH.path);
}

void display_elf()
{
	printf("magics : ELF\n");
	printf("e_ident[EI_CLASS] = %s\ne_ident[EI_DATA] = %s\n", a_arch[g_elfH.elf64.Ehdr->e_ident[4] - 1], a_endianess[g_elfH.elf64.Ehdr->e_ident[5] - 1]);
	printf("Header Table start at %p\n", g_elfH.elf64.Ehdr);
	printf("Section Header Table head.e_shoff = %#04lX\n", g_elfH.elf64.Ehdr->e_shoff);
	printf("Section start at %p\n", g_elfH.basePtr + g_elfH.elf64.Ehdr->e_shoff);
	printf("Section Header Table Size head.e_shentsize = %i\n", g_elfH.elf64.Ehdr->e_shentsize);
	printf("Section Header Table entry count = %i\n", g_elfH.elf64.Ehdr->e_shnum);
	printf("Section Header string table index %i\n", g_elfH.elf64.Ehdr->e_shstrndx);
}
